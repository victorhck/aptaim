# aptaim

Script en Bash para mostrar el uptime de nuestro sistema GNU/Linux en formato: d m s

![](aptaim.png)

Cuando el valor de los días del uptime es igual a 0, no se muestra el valor.

## Instalación

Puedes descargar este script ejecutando:

`wget https://codeberg.org/victorhck/aptaim/raw/branch/main/aptaim`

Dale permisos de ejecución ejecutando:

`chmod +x aptaim`

Después muévelo a la carpeta que desees, por ejemplo `/usr/bin` si lo quieres poder ejecutar desde cualquier sitio del sistema, y después simplemente ejecútalo como un comando más de tu sistema.
